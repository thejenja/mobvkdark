# OpenVK Mobile Dark 
Тема для OpenVK, окрашивающая мобильную тему в тёмные цвета

Доступно в Stylus: https://userstyles.world/style/2653/openvk-mobile-dark
## Примечания
- Применяйте с темой **OpenVK Mobile**
## Как установить?
- Скачайте расширение [Stylus](https://github.com/openstyles/stylus)
- Зайдите на сайт OpenVK, нажмите на расширение
- Вставьте код с этого репозитория
- Выберите внизу 
  > Применить к: URL, начинающимся с https://openvk.su/
